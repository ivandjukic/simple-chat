express = require('express')
const app = express()
const port = 3000
const server = app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
const bodyParser = require('body-parser');
const io = require('socket.io')(server);


app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.post('/messages', (req, res) => {
	io.emit('MESSAGE', req.body);
    res.end(JSON.stringify({ "status": "success" }));
});

